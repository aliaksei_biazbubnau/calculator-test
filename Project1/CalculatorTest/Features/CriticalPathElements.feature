﻿Feature: Operations with two numbers including Critical Path test data
	As user
	I want to execute operations by calculator
	And get correct result

@CriticalPath
Scenario Outline: Critical path operations
	Given I have opened calculator
	When I press <FirstElement>
	And I press <Operation>
	And I press <SecondElement>
	And I press =
	Then result on screen is <Result>

Examples: 
| FirstElement  | Operation | SecondElement  | Result |
|      	7       |    -      |       8        |   -1   |
| 		1       |    -      | 		0		 |    1   |
|       5       |    X      |       0        |    0   |
| 		10		|    ÷      | 		0		 |    ∞   |
