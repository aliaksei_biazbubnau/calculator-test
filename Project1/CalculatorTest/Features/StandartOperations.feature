﻿Feature: Calculator 
	As user
	I want to execute operations by calculator
	So result of operations should be correct

@Addition
Scenario: User enters Numbers press Add and Equals buttons and gets result
	Given I have opened calculator
	When I press 1	
	And I press +
	And I press 2
	And I press =
	Then result on screen is 3

@Subtraction
Scenario: User enters Numbers press Subtract and Equals buttons and gets result
	Given I have opened calculator
	When I press 10	
	And I press -
	And I press 2
	And I press =
	Then result on screen is 8

@Multiplication
Scenario: User enters Numbers press Multiply and Equals buttons and gets result
	Given I have opened calculator
	When I press 7	
	And I press X
	And I press 8
	And I press =
	Then result on screen is 56

@Division
Scenario: User enters Numbers press Divide and Equals buttons and gets result
	Given I have opened calculator
	When I press 32	
	And I press ÷
	And I press 4
	And I press =
	Then result on screen is 8