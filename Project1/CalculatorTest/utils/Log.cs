﻿using log4net;

namespace CalculatorTest.CalculatorTest.utils
{
    class Log
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void Info(string message)
        {
            log.Info(message);
        }

        public static void Debug(string message)
        {
            log.Debug(message);
        }

        public static void Error(string message)
        {
            log.Error(message);
        }
    }
}
