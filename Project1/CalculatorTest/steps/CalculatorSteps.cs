﻿using System;
using TechTalk.SpecFlow;
using NUnit.Framework;
using Project1.CalculatorTest.product;

namespace Project1.CalculatorTest.steps
{
    [Binding]
    public class CalculatorSteps
    {
        private Calculator calculator = new Calculator();

        [After]
        public void TearDown()
        {
            calculator.CloseApp();
        }

        [Given(@"I have opened calculator")]
        public void OpenCalculator()
        {
            calculator.LaunchApp();
        }
       
        [When(@"I press (.*)")]
        public void EnterData(String data)
        {
            calculator.TypeData(data);
        }
        
        [Then(@"result on screen is (.*)")]
        public void CheckResultOnScreen(string expectedResult)
        {
            string actualResult = calculator.GetResult();
            Assert.AreEqual(expectedResult, actualResult, "Both values should be equal");
        }
    }
}
