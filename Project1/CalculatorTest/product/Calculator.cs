﻿using System;
using TestStack.White;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using CalculatorTest.CalculatorTest.utils;

namespace Project1.CalculatorTest.product
{
    class Calculator
    {
        private Application calculator;
        private Window calculatorWindow;

        public void LaunchApp()
        {
            Log.Info("Starting Application...");
            var filePath = @"C:\Calculator.exe";
            calculator = Application.Launch(filePath);
            calculatorWindow = calculator.GetWindow("Calculator");
            Log.Info("Application is started");
        }

        public void CloseApp()
        {
            Log.Info("Closing Application...");
            calculator.Kill();
            Log.Info("Application is closed");
        }

        public void TypeData(string data)
        {
            Log.Info("Type data " + data);
            char[] targetSymbols = data.ToCharArray();
            Array.ForEach(targetSymbols, symbol => ClickOnButton(symbol.ToString()));
        }

        private void ClickOnButton(string name)
        {
            Log.Info("Click on button: " + name);
            Button targetButton = calculatorWindow.Get<Button>(SearchCriteria.ByText(name));
            targetButton.Click();
        }

        public string GetResult()
        {
            Log.Info("Get result value");
            var txtresult = calculatorWindow.Get<TextBox>(SearchCriteria.ByAutomationId("CalcText"));
            var result = txtresult.Text;
            Log.Info("Result value is: " + result);
            return result;
        }
    }
}
